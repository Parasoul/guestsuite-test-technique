<?php

namespace App\Controller;

use App\Service\ScraperService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home", methods={"GET"})
     * @param \App\Service\ScraperService $scraper
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function view(ScraperService $scraper)
    {
        $data = $scraper->getAllData();

        return $this->render('home/view.html.twig', compact('data'));
    }
}
