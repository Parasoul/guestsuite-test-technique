<?php

namespace App\Service;

class ScraperService
{
    private $rootPath;
    private $data;
    private $FILE_PATH = "data/data.json";

    public function __construct(string $rootPath)
    {
        $this->rootPath = $rootPath;
    }

    public function getAllData(): array
    {
        $data          = $this->readData();
        $formattedData = [];
        foreach ($data as $row) {
            foreach ($row->rates as $key => $rate) {
                $formattedData[$key]['rates'][] = ['date' => $row->date, 'score' => $rate];
            }
        }
        foreach ($formattedData as &$row) {
            $rates                    = array_map(function($a){return $a['score'];}, $row['rates']);
            $row['average'] = round(array_sum($rates) / count($rates), 1);
        }

        return $formattedData;
    }

    private function readData(): array
    {
        return $this->data ?? $this->data = json_decode(file_get_contents("{$this->rootPath}/{$this->FILE_PATH}"));
    }
}
