document.addEventListener("DOMContentLoaded", function() {
    let lineChart;
    let ctx = document.getElementById('line-chart');
    let sites = document.querySelectorAll('.sites');

    sites.forEach(function(site){
        site.addEventListener('click', function(){
           updateLineChart(site.dataset.key);
        });
    });

    updateLineChart( sites[0].dataset.key);

    function updateLineChart(key){
        lineChart = new Chart(ctx, {
            type : 'line',
            data : getData(key),
            options : {
                title: {
                    display: true,
                    text: 'Evolution des notes ' + key + ' jours pas sur jours'
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            suggestedMax: 10
                        }
                    }]
                }
            }
        });
    }

    function getData(key) {
        let labels = [];
        let rates = [];
        data[key].rates.forEach(function(rate){
            labels.push(rate.date);
            rates.push(rate.score);
        });
        return {
            labels : labels,
            datasets : [{
                label: 'Moyenne des notes',
                data : rates,
                borderWidth : 1
            }]
        };
    }
});